function bubbleSort(data) {
  for(var i = 0; i < data.length-1; i++){
    for(var j = i + 1; j < data.length; j++){
      if(data[j]<data[i]){
        var holder = data[i];
        data[i] = data[j];
        data[j] = holder;
      }
    }
  }
  console.log("bubble sort",data);
}

function selectionSort(data) {
  var min,pos;
  for(var i = 0; i <= data.length-2; i++){
    pos = i;
    for(var j = i+1; j < data.length; j++){
      if(data[j]<data[pos]){
        pos = j;
      }
    }
    min = data[pos];
    data[pos] = data[i];
    data[i] = min;
  }
  console.log("selection sort",data);
}

function insertionSort(data) {
  for(var i = 1; i < data.length; i++){
    var current = data[i];
    var j = i - 1;
    while(j >= 0 && data[j] > current){
      data[j+1] = data[j];
      j = j - 1;
    }
    data[j+1]=current;
  }
  console.log("insertion sort",data);
}

function quickSort(data){
  if(data.length <= 1){
    return data;
  }
  var temp = data[0];
  var lesser = [];
  var greater = [];
  for(var i = 1;i < data.length;i++){
    if(data[i]<temp){
      lesser.push(data[i]);
    }else{
      greater.push(data[i]);
    }
  }
  return quickSort(lesser).concat(temp,quickSort(greater));
}
console.log("quick sort",quickSort([2,3,1,9,6,5,0]));

bubbleSort([2,3,1,9,6,5,0]);
selectionSort([2,3,1,9,6,5,0]);
insertionSort([2,3,1,9,6,5,0]);
