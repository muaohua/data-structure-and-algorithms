function binarySearch(target, data){
  var start = 0;
  var end = data.length-1;
  while(start <= end){
    var middle = Math.floor((start+end)/2);
    if(target==data[middle]){
      return "Found Target "+target+" in "+middle;
    }
    if(target < data[middle]){
      end = middle - 1;
    }else{
      start = middle + 1;
    }
  }
  return "Not Found!"
}

console.log(binarySearch(900,[1,4,5,6,7,9,10,20,300,400,450]));
