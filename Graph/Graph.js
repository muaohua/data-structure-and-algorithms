function Vertex(data){
    this.data = data;
}

function Graph(v){
    this.vertices = v;
    this.edges = 0;
    this.adj = [];
    this.marked=[];
    for(var i = 0; i < this.vertices;i++){
        this.adj[i]=[];
        this.marked[i]=false;
    }
}

Graph.prototype.reset = function(){
    for(var i = 0; i < this.vertices;i++){
        this.marked[i]=false;
    }
}

Graph.prototype.addEdge = function(v, w){
    this.adj[v].push(w);
    this.adj[w].push(v);
    this.edges++;
}

Graph.prototype.showGraph = function(){
    for(var i = 0; i < this.vertices; i++){
        var vertexStr = i + "-->";   
        for(var j = 0; j < this.adj[i].length; j++){
            vertexStr += " "+this.adj[i][j];
        }
        console.log(vertexStr);
    }
}

Graph.prototype.dfsRecursive = function(v){
        this.marked[v] = true;
        console.log("Visited vertex:"+v);
        for(var i = 0; i < this.adj[v].length; i++){
            if(!this.marked[this.adj[v][i]]){
                this.dfsRecursive(this.adj[v][i]);
            }
        }    
}

Graph.prototype.dfs = function(v){
    this.marked[v] = true;
    var stack = [];
    stack.push(v);
    while(stack.length > 0){
        var current = stack.pop();
        console.log("Visited vertex:"+current);
        for(var i = 0; i < this.adj[current].length; i++){
            if(!this.marked[this.adj[current][i]]){
                this.marked[this.adj[current][i]] = true;
                stack.push(this.adj[current][i]);
            }
        }
    }
}

Graph.prototype.bfs = function(v){
    this.marked[v]=true;
    var queue = [];
    queue.push(v);
    while(queue.length > 0){
        var current = queue.shift();
        console.log("Visited vertex:"+current);
        for(var w in this.adj[current]){
            if(!this.marked[this.adj[current][w]]){
                this.marked[this.adj[current][w]] = true;
                queue.push(this.adj[current][w]);
            }
        }
    }
}


var graph = new Graph(5);
graph.addEdge(0,1);
graph.addEdge(0,2);
graph.addEdge(1,3);
graph.addEdge(2,4);
graph.showGraph();
console.log("dfs");
graph.dfsRecursive(0);
graph.reset();
console.log("dfs none recursive");
graph.dfs(0);
graph.reset();
console.log("bfs");
graph.bfs(0);