var Node = function(data, left, right) {
  this.data = data;
  this.left = left;
  this.right = right;
}

Node.prototype.find = function(data){
    if(data > this.data && this.right != null){
        return this.right.find(data);
    }else if(data < this.data && this.left != null){
        return this.left.find(data);
    }else if(data == this.data){
        return "Found: "+this.data;
    }
    return data + " Not Found!";
}

var BinarySearchTree = function() {
  this.root = null;
}

BinarySearchTree.prototype.insert = function(data) {
  var n = new Node(data, null, null);

  if (this.root == null) {
    this.root = n;
  } else {
    var current = this.root;
    var parent;
    while (true) {
      parent = current;
      if (data > current.data) {
        current = parent.right;
        if (current == null) {
          parent.right = n;
          break;
        }
      } else {
        current = parent.left;
        if (current == null) {
          parent.left = n;
          break;
        }
      }
    }
  }
}

BinarySearchTree.prototype.find = function(data){
    return this.root.find(data);
}

BinarySearchTree.prototype.inOrder = function(node){
  if(node!=null){
    this.inOrder(node.left);
    console.log(node.data);
    this.inOrder(node.right);
  }
}

BinarySearchTree.prototype.preOrder = function(node){
  if(node!=null){
    console.log(node.data);
    this.preOrder(node.left);
    this.preOrder(node.right);
  }
}

BinarySearchTree.prototype.postOrder = function(node){
  if(node!=null){
    this.postOrder(node.left);
    this.postOrder(node.right);
    console.log(node.data);
  }
}

BinarySearchTree.prototype.max = function(){
    var current = this.root;
    if(current == null){
        return null;
    }
    while(true){
        if(current.right != null){
            current = current.right;
        }else{
            return current;
        }
    }
}

BinarySearchTree.prototype.min = function(){
    var current = this.root;
    if(current == null){
        return null;
    }
    while(true){
        if(current.left != null){
            current = current.left;
        }else{
            return current;
        }
    }
}

BinarySearchTree.prototype.findLastLeftWithParent = function(node){
    var current = node;
    var parent = node;
    while(true){
        if(current.left != null){
            parent = current;
            current = current.left;
        }else{
            return {"parent": parent,"last": current};
        }
    }
}

BinarySearchTree.prototype.remove = function(data){
    var parent = this.root;
    var current = this.root;
    var isLeft = false;
    if(current == null){
        return;
    }
    while(current!=null&&current.data!=data){
        parent = current;
        
        if(data > current.data){
            current = parent.right;
            isLeft = false;
        }else if(data < current.data){
            current = parent.left;
            isLeft = true;
        }
    }
    if(current==null){
        return;
    } 
    if(current.left == null && current.right == null){
        if(current.data == this.root.data){
            this.root = null;
        }else if(isLeft){
            parent.left=null;
        }else{
            parent.right=null;
        }
    }else if(current.left==null){
         if(current.data == this.root.data){
             this.root = this.root.right;
         }else if(isLeft){
            parent.left = current.right;
        }else{
            parent.right = current.right;
        }
    }else if(current.right==null){
        if(current.data == this.root.data){
             this.root = this.root.left;
         }else if(isLeft){
            parent.left = current.left;
        }else{
            parent.right = current.left;
        }
    }else{
        
        if(current.data == this.root.data){
            var lastNode = this.findLastLeftWithParent(current.right);
            var lastLeft = lastNode.last;
            var lastParent = lastNode.parent;
            lastParent.left = null;
            lastLeft.right = this.root.right;
            lastLeft.left = this.root.left;
            this.root = lastLeft;
        }else if(isLeft){
            var lastNode = this.findLastLeftWithParent(current);
            var lastLeft = lastNode.last;
            var lastParent = lastNode.parent;
            lastParent.left = null;
            lastLeft.right = current.right;
            lastLeft.left = current.left;
            parent.left = lastLeft;
        }else{
            var lastNode = this.findLastLeftWithParent(current);
            var lastLeft = lastNode.last;
            var lastParent = lastNode.parent;
            lastParent.left = null;
            lastLeft.right = current.right;
            lastLeft.left = current.left;
            parent.right = lastLeft;
        }
    }
}

//init binary search tree
var binarySearchTree = new BinarySearchTree();
binarySearchTree.insert(23);
binarySearchTree.insert(45);
binarySearchTree.insert(16);
binarySearchTree.insert(37);
binarySearchTree.insert(3);
binarySearchTree.insert(99);
binarySearchTree.insert(22);

console.log("In Order:");
binarySearchTree.inOrder(binarySearchTree.root);
//remove 99
console.log("remove 45");
binarySearchTree.remove(45);
binarySearchTree.inOrder(binarySearchTree.root);
console.log("Preorder:");
binarySearchTree.preOrder(binarySearchTree.root);
console.log("Post Order:");
binarySearchTree.postOrder(binarySearchTree.root);

console.log(binarySearchTree.find(45));
console.log(binarySearchTree.find(3));
console.log(binarySearchTree.find(4));

//get max
console.log(binarySearchTree.max().data);
//get min
console.log(binarySearchTree.min().data);
